package com.chuanye.loca.management.controller;


import com.chuanye.loca.management.model.Locavideo;
import com.chuanye.loca.management.request.LocavideoGetByPageRequest;
import com.chuanye.loca.management.request.LocavideoGetRequest;
import com.chuanye.loca.management.response.LocavideoModel;
import com.chuanye.loca.management.service.LocavideoService;
import com.chuanye.loca.management.util.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "locavideo",description = "站点视频",tags = {"LocaVideo"})
@RestController
@RequestMapping("/locavideo")
public class LocaVideoController {

    @Autowired
    private LocavideoService service;

    @ApiOperation(value = "增加")
    @PostMapping("/add")
    public ResultDtoBase add(@RequestBody Locavideo obj) {
        ResultDtoBase result=new ResultDtoBase();
        try {
            if(StringUtil.isNullOrEmpty(obj.getVideoName()))
            {
                result.setStatus(-1);
                result.setMessage("VideoName is null");
            }
            obj.setVideoId(GuidUtil.newGuid());
            int status=service.add(obj);
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locavideo/add Server Error");
        }
        return result;
    }

    @ApiOperation(value = "删除")
    @PostMapping("/remove")
    public  ResultDtoBase remove(@RequestBody LocavideoGetRequest obj){
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.remove(obj.getVideoId());
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locavideo/remove Server Error");
        }
        return result;
    }

    @ApiOperation(value = "编辑")
    @PostMapping("/modify")
    public  ResultDtoBase modify(@RequestBody Locavideo obj) {
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.modify(obj);
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locavideo/modify Server Error");
        }
        return result;
    }

    @ApiOperation(value = "获取单一")
    @PostMapping("/get")
    public ResultFirstDto<LocavideoModel> getByLocavideoId(@RequestBody LocavideoGetRequest obj) {
        ResultFirstDto<LocavideoModel> result=new ResultFirstDto<LocavideoModel>();
        try {
            LocavideoModel info=service.get(obj.getVideoId());
            result.setFirstParam(info);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locavideo/get Server Error");
        }
        return result;
    }

    @ApiOperation(value = "获取列表")
    @PostMapping("/getbypage")
    public ResultSecondDto<List<LocavideoModel>,Integer> getByPage(@RequestBody LocavideoGetByPageRequest obj) {
        ResultSecondDto<List<LocavideoModel>,Integer> result=new ResultSecondDto<>();
        try
        {
            RequestPageBase page = Pickout.InitPageBase(obj);
            List<LocavideoModel> infos=service.getByPage(obj.getVideoId(),obj.getLocaId(),page);
            result.setFirstParam(infos);
            result.setSecondParam(page.getTotalRows());
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locavideo/getByPage Server Error");
        }
        return result;
    }




}
