package com.chuanye.loca.management.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

public class Alarmignoretime {

  private long ignoreId;
  private String locaId;
  private java.sql.Timestamp ignoreTime;
  private long value;

  public long getIgnoreId() {
    return ignoreId;
  }

  public void setIgnoreId(long ignoreId) {
    this.ignoreId = ignoreId;
  }

  public String getLocaId() {
    return locaId;
  }

  public void setLocaId(String locaId) {
    this.locaId = locaId;
  }


  @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
  @JsonFormat(
          pattern = "yyyy-MM-dd HH:mm:ss",
          timezone = "GMT+8",
          shape = JsonFormat.Shape.STRING,
          locale ="zh"
  )
  @ApiModelProperty(value ="增加时间",example = "yyyy-MM-dd HH:mm:ss")
  public java.sql.Timestamp getIgnoreTime() {
    return ignoreTime;
  }

  public void setIgnoreTime(java.sql.Timestamp ignoreTime) {
    this.ignoreTime = ignoreTime;
  }

  public long getValue() {
    return value;
  }

  public void setValue(long value) {
    this.value = value;
  }

}
