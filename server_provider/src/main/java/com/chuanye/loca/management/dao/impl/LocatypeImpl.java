package com.chuanye.loca.management.dao.impl;

import com.chuanye.loca.management.dao.JdbcTemplateBase;
import com.chuanye.loca.management.dao.LocatypeDao;
import com.chuanye.loca.management.model.Locatype;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Primary
class LocatypeImpl extends JdbcTemplateBase implements LocatypeDao {
    public List<Locatype> get(String locaTypeID, String locaTypeName) throws Exception {
        StringBuilder sql=new  StringBuilder() ;
        sql.append(" select * from locatype  where 1=1");
        if(locaTypeID != null && locaTypeID.length() != 0)
        {
            sql.append(" and `LocaTypeID`='"+locaTypeID+"'");
        }
        if(locaTypeName != null && locaTypeName.length() != 0)
        {
            sql.append(" and `LocaTypeName`='"+locaTypeName+"'");
        }
        sql.append(" order by `locaTypeIndex` desc ");
        List<Locatype> sysInfos = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<Locatype>(Locatype.class));
        return sysInfos;
    }

    public int add(Locatype info) throws Exception {
        String sql=insertSql("locatype",info);
        return update(sql);
    }

    public int modify(Locatype info) throws Exception {
        String sql=updateSql("locatype",info,new String[]{"SysID"});
        return update(sql);
    }

    public int remove(String locaTypeID) throws Exception {
        Map<String,String> map=new HashMap<String, String>();
        map.put("LocaTypeID",locaTypeID);
        String sql=deleteSql("locatype",map);
        return update(sql);
    }
}
