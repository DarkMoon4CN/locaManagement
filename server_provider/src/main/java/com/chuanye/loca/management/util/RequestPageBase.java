package com.chuanye.loca.management.util;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

public class RequestPageBase {


    public int getPageIndex() {
        return PageIndex;
    }

    public void setPageIndex(int pageIndex) {
        PageIndex = pageIndex;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    @ApiModelProperty(value = "查询时间范围")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getStartTime() {
        return StartTime;
    }

    public void setStartTime(Date startTime) {
        StartTime = startTime;
    }

    @ApiModelProperty(value = "查询时间范围")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getEndTime() {
        return EndTime;
    }

    public void setEndTime(Date endTime) {
        EndTime = endTime;
    }

    public String getKeyword() {
        return Keyword;
    }

    public void setKeyword(String keyword) {
        Keyword = keyword;
    }

    public int getTotalRows() {
        return TotalRows;
    }

    public void setTotalRows(int totalRows) {
        TotalRows = totalRows;
    }

    public int getOrderByPattern() {
        return OrderByPattern;
    }

    public void setOrderByPattern(int orderByPattern) {
        OrderByPattern = orderByPattern;
    }

    public String getOrderByKey() {
        return OrderByKey;
    }

    public void setOrderByKey(String orderByKey) {
        OrderByKey = orderByKey;
    }
    private int PageIndex;
    private int PageSize;
    private java.util.Date StartTime;
    private java.util.Date EndTime;
    private String Keyword;
    private int TotalRows;
    private int OrderByPattern;
    private String OrderByKey;

}
