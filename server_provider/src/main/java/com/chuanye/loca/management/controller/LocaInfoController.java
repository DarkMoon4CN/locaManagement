package com.chuanye.loca.management.controller;


import com.chuanye.loca.management.model.Locainfo;
import com.chuanye.loca.management.request.LocainfoGetByPageRequest;
import com.chuanye.loca.management.request.LocainfoGetByTreeRequest;
import com.chuanye.loca.management.request.LocainfoGetRequest;
import com.chuanye.loca.management.response.LocaInfoModel;
import com.chuanye.loca.management.response.LocainfoTreeModel;
import com.chuanye.loca.management.service.LocainfoService;
import com.chuanye.loca.management.util.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "locainfo",description = "站点信息",tags = {"LocaInfo"})
@RestController
@RequestMapping("/locainfo")
public class LocaInfoController {

    @Autowired
    private LocainfoService service;

    @ApiOperation(value = "增加")
    @PostMapping("/add")
    public ResultDtoBase add(@RequestBody Locainfo obj) {
        ResultDtoBase result=new ResultDtoBase();
        try {
            obj.setLocaId(GuidUtil.newGuid());
            int status=service.add(obj);
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locainfo/add Server Error");
        }
        return result;
    }

    @ApiOperation(value = "删除")
    @PostMapping("/remove")
    public  ResultDtoBase remove(@RequestBody LocainfoGetRequest obj){
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.remove(obj.getLocaId());
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locainfo/remove Server Error");
        }
        return result;
    }

    @ApiOperation(value = "编辑")
    @PostMapping("/modify")
    public  ResultDtoBase modify(@RequestBody Locainfo obj) {
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.modify(obj);
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locainfo/modify Server Error");
        }
        return result;
    }

    @ApiOperation(value = "获取单一")
    @PostMapping("/get")
    public ResultFirstDto<LocaInfoModel> getBylocaId(@RequestBody LocainfoGetRequest obj) {
        ResultFirstDto<LocaInfoModel> result=new ResultFirstDto<LocaInfoModel>();
        try {
            LocaInfoModel info=service.get(obj.getLocaId());
            result.setFirstParam(info);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locainfo/get Server Error");
        }
        return result;
    }
    @ApiOperation(value = "获取列表")
    @PostMapping("/getbypage")
    public ResultSecondDto<List<LocaInfoModel>,Integer> getByPage(@RequestBody LocainfoGetByPageRequest obj) {
        ResultSecondDto<List<LocaInfoModel>,Integer> result=new ResultSecondDto<List<LocaInfoModel>,Integer>();
        try
        {
            RequestPageBase page = Pickout.InitPageBase(obj);
            List<LocaInfoModel> info=service.getByPage(obj.getLocaId(),obj.getAddressCounty(),page);
            result.setFirstParam(info);
            result.setSecondParam(page.getTotalRows());
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locainfo/get Server Error");
        }
        return result;
    }

    @ApiOperation(value = "获取基站树形结构")
    @PostMapping("/getbytree")
    public ResultFirstDto<List<LocainfoTreeModel>> getByTree(@RequestBody LocainfoGetByTreeRequest obj) {
        ResultFirstDto<List<LocainfoTreeModel>> result=new ResultFirstDto<List<LocainfoTreeModel>>();
        try {
            List<LocainfoTreeModel> infos=service.getByTree(obj.getAddressCounty());
            result.setFirstParam(infos);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locainfo/getbytree Server Error");
        }
        return result;
    }
}
