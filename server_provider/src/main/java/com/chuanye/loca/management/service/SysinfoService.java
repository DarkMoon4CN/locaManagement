package com.chuanye.loca.management.service;

import com.chuanye.loca.management.dao.SysinfoDao;
import com.chuanye.loca.management.model.Sysinfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SysinfoService {

    @Resource
    private SysinfoDao dao;

    public List<Sysinfo> get(String sysId, String sysName) throws Exception {
        return dao.get(sysId,sysName);
    }
    public int add(Sysinfo info) throws Exception{
        return dao.add(info);
    }

    public int modify(Sysinfo info) throws Exception {
        return dao.modify(info);
    }

    public int remove(String sysID)throws Exception{
        return dao.remove(sysID);
    }
}
