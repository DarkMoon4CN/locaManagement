package com.chuanye.loca.management.controller;


import com.chuanye.loca.management.model.Alarmignoretime;
import com.chuanye.loca.management.model.Videoalarm;
import com.chuanye.loca.management.request.VideoalarmGetByPageRequest;
import com.chuanye.loca.management.request.VideoalarmGetRequest;
import com.chuanye.loca.management.request.VideoalarmchangeAuditStateRequest;
import com.chuanye.loca.management.response.AlarmignoretimeModel;
import com.chuanye.loca.management.response.VideoalarmModel;
import com.chuanye.loca.management.service.AlarmignoretimeService;
import com.chuanye.loca.management.service.VideoalarmService;
import com.chuanye.loca.management.util.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "videoalarm",description = "视频告警",tags = {"VideoAlarm"})
@RestController
@RequestMapping("/videoalarm")
public class VideoAlarmController {
    @Autowired
    private VideoalarmService service;

    @Autowired
    private AlarmignoretimeService agService;

    @ApiOperation(value = "增加")
    @PostMapping("/add")
    public ResultDtoBase add(@RequestBody Videoalarm obj) {
        ResultDtoBase result=new ResultDtoBase();
        try {
            obj.setAlarmId(GuidUtil.newGuid());
            int status=service.add(obj);
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/videoalarm/add Server Error");
        }
        return result;
    }

    @ApiOperation(value = "删除")
    @PostMapping("/remove")
    public  ResultDtoBase remove(@RequestBody VideoalarmGetRequest obj){
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.remove(obj.getAlarmId());
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/videoalarm/remove Server Error");
        }
        return result;
    }

    @ApiOperation(value = "更改审核状态")
    @PostMapping("/changeauditstate")
    public  ResultDtoBase changeAuditState(@RequestBody VideoalarmchangeAuditStateRequest obj) {
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.changeAuditState(obj.getAlarmId(),obj.getAuditState());
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/videoalarm/changeAuditState Server Error");
        }
        return result;
    }


    @ApiOperation(value = "编辑")
    @PostMapping("/modify")
    public  ResultDtoBase modify(@RequestBody Videoalarm obj) {
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.modify(obj);
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/videoalarm/modify Server Error");
        }
        return result;
    }




    @ApiOperation(value = "获取单一")
    @PostMapping("/get")
    public ResultFirstDto<VideoalarmModel> getByAlarmId(@RequestBody VideoalarmGetRequest obj) {
        ResultFirstDto<VideoalarmModel> result=new ResultFirstDto<>();
        try {
            VideoalarmModel info=service.get(obj.getAlarmId());
            result.setFirstParam(info);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/videoalarm/get Server Error");
        }
        return result;
    }

    @ApiOperation(value = "获取列表")
    @PostMapping("/getbypage")
    public ResultSecondDto<List<VideoalarmModel>,Integer> getByPage(@RequestBody VideoalarmGetByPageRequest obj) {
        ResultSecondDto<List<VideoalarmModel>,Integer> result=new ResultSecondDto<List<VideoalarmModel>,Integer>();
        try
        {
            RequestPageBase page = Pickout.InitPageBase(obj);
            List<VideoalarmModel> infos=service.getByPage(obj.getAlarmId(),obj.getLocaId(),obj.getAddressCounty(),obj.getAddressTown(),obj.getAddressVillage(),page);
            result.setFirstParam(infos);
            result.setSecondParam(page.getTotalRows());
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/videoalarm/get Server Error");
        }
        return result;
    }


    @ApiOperation(value = " 设置站点下忽略时间")
    @PostMapping("/addalarmignoretime")
    public  ResultDtoBase addAlarmIgnoreTime(@RequestBody Alarmignoretime obj) {
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=agService.addAlarmignoretime(obj);
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/videoalarm/addalarmignoretime Server Error");
        }
        return result;
    }


    @ApiOperation(value = "获取站点下忽略提醒配置")
    @PostMapping("/getalarmignoretime")
    public ResultFirstDto<List<AlarmignoretimeModel>> getAlarmIgnoreTime() {
        ResultFirstDto<List<AlarmignoretimeModel>> result=new ResultFirstDto<>();
        try {
            List<AlarmignoretimeModel> info=agService.get();
            result.setFirstParam(info);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/videoalarm/getalarmignoretime Server Error");
        }
        return result;
    }



}
