package com.chuanye.loca.management.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleClassUtil {
    public static Object getFieldValueByName(String fieldName,Object entity) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = entity.getClass().getMethod(getter, new Class[] {});
            Object value = method.invoke(entity, new Object[] {});
            return value;
        } catch (Exception e) {
            return null;
        }
    }

    public static List<Map<String,String>> getFiledsInfo(Object entity){
        Field[] fields=entity.getClass().getDeclaredFields();
        String[] fieldNames=new String[fields.length];
        List<Map<String,String>> list = new ArrayList<>();
        for(int i=0;i<fields.length;i++){
            Map infoMap = new HashMap();
            infoMap.put("type", fields[i].getType().toString());
            infoMap.put("name", fields[i].getName());
            infoMap.put("value", getFieldValueByName(fields[i].getName(), entity));
            list.add(infoMap);
        }
        return list;
    }
}
