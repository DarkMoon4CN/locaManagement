package com.chuanye.loca.management.model;


import com.chuanye.loca.management.util.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "请求体")
public class Locatype {

    @ApiModelProperty(value = "主键，增加时忽略")
    public String getLocaTypeId() {
        return locaTypeId;
    }

    public void setLocaTypeId(String locaTypeId) {
        this.locaTypeId = locaTypeId;
    }

    @ApiModelProperty(value = "站点类型名称")
    public String getLocaTypeName() {
        return locaTypeName;
    }

    public void setLocaTypeName(String locaTypeName) {
        this.locaTypeName = locaTypeName;
    }

    @ApiModelProperty(value = "排序")
    public int getLocaTypeIndex() {
        return locaTypeIndex;
    }

    public void setLocaTypeIndex(int locaTypeIndex) {
        this.locaTypeIndex = locaTypeIndex;
    }

    @ApiModelProperty(value = "分类封面图片地址")
    public String getLocaImgPath() {
        return locaImgPath;
    }

    public void setLocaImgPath(String locaImgPath) {
        this.locaImgPath = locaImgPath;
    }

    private String locaTypeId;
    private String locaTypeName;
    private int locaTypeIndex;
    private String locaImgPath;
}
