package com.chuanye.loca.management.dao.impl;

import com.chuanye.loca.management.dao.JdbcTemplateBase;
import com.chuanye.loca.management.dao.VideoalarmDao;
import com.chuanye.loca.management.model.Videoalarm;
import com.chuanye.loca.management.response.VideoalarmModel;
import com.chuanye.loca.management.util.RequestPageBase;
import com.chuanye.loca.management.util.SimpleMySqlUtil;
import com.chuanye.loca.management.util.StringUtil;
import com.google.common.collect.Table;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.swing.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Primary
class VideoalarmImpl extends JdbcTemplateBase implements VideoalarmDao {
    public int getAlarmCountBynUnits(List<Long> nUnits) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select count(1) as `Count` from  videoalarm  where 1=1");
        sql.append(SimpleMySqlUtil.in(nUnits,"NUnit"));
        int count = jdbcTemplate.queryForObject(sql.toString(), Integer.class);
        return count;
    }

    public int add(Videoalarm info) throws Exception {
        String sql=insertSql("videoalarm",info);
        return update(sql);
    }

    public int modify(Videoalarm info) throws Exception {
        String sql=updateSql("videoalarm",info,new String[]{"AlarmID"});
        return update(sql);
    }

    public int remove(String alarmId) throws Exception {
        Map<String,String> map=new HashMap<String, String>();
        map.put("AlarmID",alarmId);
        String sql=deleteSql("videoalarm",map);
        return update(sql);
    }

    public VideoalarmModel get(String alarmId) throws Exception {
        List<VideoalarmModel> locaVideoModels=getByPage(alarmId,null,null,null,null,null);
        return locaVideoModels!=null && locaVideoModels.size()>0?locaVideoModels.get(0):null;
    }

    public List<VideoalarmModel> getByPage(String alarmId, String locaId, String addressCounty, String addressTown, String addressVillage, RequestPageBase page) throws Exception {
        String sql = " select a.*,b.VideoName,b.VideoPath,c.LocaName,c.AddressCounty,c.AddressTown,c.AddressVillage  ";
        sql += " ,d.AlarmTypeName,e.AuditStateName ";
        sql += " from videoalarm as a";
        sql += " left join locavideo as b on a.nUnit = b.nUnit ";
        sql += " left join locainfo as c on a.LocaID =c.LocaID ";
        sql += " left join videoalarmtype as d on a.AlarmType =d.AlarmType ";
        sql += " left join videoalarmauditstate as e on a.AuditState =e.AuditState ";
        sql += " where 1=1 ";
        if (!StringUtil.isNullOrEmpty(alarmId))
        {
            sql += " and a.AlarmID = '%s'";
            sql = String.format(sql, alarmId);
        }
        if (!StringUtil.isNullOrEmpty(locaId))
        {
            sql += " and c.LocaID = '%s'";
            sql = String.format(sql, locaId);
        }
        if (!StringUtil.isNullOrEmpty(addressCounty))
        {
            sql += " and c.AddressCounty = '%s'";
            sql = String.format(sql, addressCounty);
        }
        if (!StringUtil.isNullOrEmpty(addressTown))
        {
            sql += " and c.AddressTown = '%s'";
            sql = String.format(sql, addressTown);
        }
        if (!StringUtil.isNullOrEmpty(addressVillage))
        {
            sql += " and c.AddressVillage = '%s'";
            sql = String.format(sql, addressVillage);
        }

        if(page!=null)
        {
            if (!StringUtil.isNullOrEmpty(page.getKeyword())) {
                String paramsSql=new String();
                paramsSql += " a.`LocaID` like  '%"+page.getKeyword()+"%'";
                paramsSql += " or c.`LocaName` like '%"+page.getKeyword()+"%'";
                paramsSql += " or b.`VideoName` like '%"+page.getKeyword()+"%' ";
                sql +=String.format(" and (%s) ",paramsSql);

            }
            String timeSql = SimpleMySqlUtil.TimeSql(page," a.`AlarmTime` ") ;
            sql +=timeSql;
            String countSql = SimpleMySqlUtil.CountSql(sql);
            int count=jdbcTemplate.queryForObject(countSql, Integer.class);
            page.setTotalRows(count);
            sql += SimpleMySqlUtil.OrderBySql(page,"a.AlarmTime");
            sql +=SimpleMySqlUtil.limit(page);
        }
        List<VideoalarmModel> videoalarmModels = jdbcTemplate.query(sql, new BeanPropertyRowMapper<VideoalarmModel>(VideoalarmModel.class));
        return videoalarmModels;
    }

    public int changeAuditState(String alarmId, int auditState) throws Exception {
        String paramsSql=new String();
        if (!StringUtil.isNullOrEmpty(alarmId))
        {
            paramsSql += " and AlarmID = '%s'";
            paramsSql = String.format(paramsSql, alarmId);
        }
        String sql=selectSql("videoalarm",paramsSql);

        List<Videoalarm> videoalarmModels = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Videoalarm>(Videoalarm.class));
        if(videoalarmModels!=null && videoalarmModels.size() >0)
        {
            Videoalarm videoalarm=videoalarmModels.get(0);
            videoalarm.setAuditState(auditState);
            String updateSql=updateSql("videoalarm",videoalarm,new String[]{"AlarmID"});
            return update(updateSql);
        }
        return 0;
    }
}
