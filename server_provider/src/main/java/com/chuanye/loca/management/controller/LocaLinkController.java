package com.chuanye.loca.management.controller;


import com.chuanye.loca.management.model.Localink;
import com.chuanye.loca.management.request.LocalinkGetRequest;
import com.chuanye.loca.management.service.LocalinkService;
import com.chuanye.loca.management.util.ResultFirstDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "localink",description = "站点链路",tags = {"LocaLink"})
@RestController
@RequestMapping("/localink")
public class LocaLinkController {

    @Autowired
    private LocalinkService service;

    @ApiOperation(value = "获取单一")
    @PostMapping("/get")
    public ResultFirstDto<Localink> getByLink(@RequestBody LocalinkGetRequest obj) {
        ResultFirstDto<Localink> result=new ResultFirstDto<>();
        try {
            List<Localink> infos=service.get(obj.getLink());
            result.setFirstParam(infos!=null &&infos.size()>0?infos.get(0):null);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/localink/get Server Error");
        }
        return result;
    }

    @ApiOperation(value = "获取全部")
    @PostMapping("/getall")
    public ResultFirstDto<List<Localink>> getAll() {
        ResultFirstDto<List<Localink>> result=new ResultFirstDto<>();
        try {
            List<Localink> infos=service.get(0);
            result.setFirstParam(infos);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/localink/getall Server Error");
        }
        return result;
    }

}
