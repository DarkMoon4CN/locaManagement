package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.model.Locasys;

import java.util.List;

public interface LocasysDao {
     int add(Locasys info)throws Exception;
     int remove(String locaSysId)throws Exception;
     int remove(String locaId,String sysId)throws Exception;
     List<Locasys> get(String locaId,String sysId)throws Exception;
}
