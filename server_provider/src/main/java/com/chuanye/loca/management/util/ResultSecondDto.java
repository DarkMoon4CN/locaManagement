package com.chuanye.loca.management.util;

public class ResultSecondDto<T1,T2> extends ResultFirstDto<T1>{
    private T2 secondParam;
    /**
     * @return the SecondParam
     */
    public T2 getSecondParam() {
        return secondParam;
    }

    /**
     * @param secondParam the SecondParam to count
     */
    public void setSecondParam(T2 secondParam) {
        this.secondParam = secondParam;
    }
}
