package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.model.Locatype;

import java.util.List;

public interface LocatypeDao {
        List<Locatype> get(String locaTypeID, String locaTypeName) throws Exception;
        int add(Locatype info) throws Exception;
        int modify(Locatype info) throws Exception;
        int remove(String locaTypeID)throws Exception;
}
