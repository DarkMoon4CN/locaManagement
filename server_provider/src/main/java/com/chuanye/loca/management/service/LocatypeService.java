package com.chuanye.loca.management.service;

import com.chuanye.loca.management.dao.LocatypeDao;
import com.chuanye.loca.management.model.Locatype;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LocatypeService {

    @Resource
    private LocatypeDao dao;
    public List<Locatype> get(String locaTypeId, String locaTypeName) throws Exception {
        return dao.get(locaTypeId,locaTypeName);
    }
    public int add(Locatype info) throws Exception{
        return dao.add(info);
    }

    public int modify(Locatype info) throws Exception {
        return dao.modify(info);
    }
    public int remove(String locaTypId)throws Exception{
        return dao.remove(locaTypId);
    }
}
