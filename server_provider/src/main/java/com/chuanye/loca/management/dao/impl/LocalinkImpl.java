package com.chuanye.loca.management.dao.impl;

import com.chuanye.loca.management.dao.JdbcTemplateBase;
import com.chuanye.loca.management.dao.LocalinkDao;
import com.chuanye.loca.management.model.Localink;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Primary
class LocalinkImpl extends JdbcTemplateBase implements LocalinkDao {
    public List<Localink> get(long link)throws Exception{
        StringBuilder sql=new  StringBuilder() ;
        sql.append(" select * from localink  where 1=1");
        if(link > 0)
        {
            sql.append(" and `Link`='"+link+"'");
        }
        sql.append(" order by `Link` asc ");
        List<Localink> localinks = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<Localink>(Localink.class));
        return localinks;
    }
}
