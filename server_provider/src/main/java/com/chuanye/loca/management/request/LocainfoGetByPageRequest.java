package com.chuanye.loca.management.request;

import com.chuanye.loca.management.util.RequestPageBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class LocainfoGetByPageRequest extends RequestPageBase {

    private  String addressCounty;
    private  String locaId;

    @ApiModelProperty(value = "所在大区")
    public String getAddressCounty() {
        return addressCounty;
    }
    public void setAddressCounty(String addressCounty) {
        this.addressCounty = addressCounty;
    }

    @ApiModelProperty(value = "站点编号")
    public String getLocaId() {
        return locaId;
    }
    public void setLocaId(String locaId) {
        this.locaId = locaId;
    }
}
