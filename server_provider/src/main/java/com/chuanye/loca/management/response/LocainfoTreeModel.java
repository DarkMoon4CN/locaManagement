package com.chuanye.loca.management.response;

import java.util.List;

public class LocainfoTreeModel {


    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public List<LocainfoChildTreeModel> getChild() {
        return child;
    }

    public void setChild(List<LocainfoChildTreeModel> child) {
        this.child = child;
    }

    private String nodeId ;
    private String nodeName ;
    private List<LocainfoChildTreeModel> child;
}
