package com.chuanye.loca.management.util;

import java.util.UUID;

public class GuidUtil {
    public static String newGuid(){
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
