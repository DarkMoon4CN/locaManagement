package com.chuanye.loca.management.request;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "请求体")
public class LocainfoGetByTreeRequest {
    private  String addressCounty;

    @ApiModelProperty(value = "所在大区")
    public String getAddressCounty() {
        return addressCounty;
    }

    public void setAddressCounty(String addressCounty) {
        this.addressCounty = addressCounty;
    }
}
