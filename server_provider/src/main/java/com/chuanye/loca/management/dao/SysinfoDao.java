package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.model.Sysinfo;

import java.util.List;

public  interface SysinfoDao {
     List<Sysinfo> get(String sysId, String sysName) throws Exception;
     int add(Sysinfo info) throws Exception;
     int modify(Sysinfo info) throws Exception;
     int remove(String sysID)throws Exception;
}
