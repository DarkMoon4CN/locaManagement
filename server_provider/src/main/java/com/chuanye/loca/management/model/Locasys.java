package com.chuanye.loca.management.model;


public class Locasys {

  private String locaSysId;
  private String locaId;
  private String sysId;


  public String getLocaSysId() {
    return locaSysId;
  }

  public void setLocaSysId(String locaSysId) {
    this.locaSysId = locaSysId;
  }


  public String getLocaId() {
    return locaId;
  }

  public void setLocaId(String locaId) {
    this.locaId = locaId;
  }


  public String getSysId() {
    return sysId;
  }

  public void setSysId(String sysId) {
    this.sysId = sysId;
  }

}
