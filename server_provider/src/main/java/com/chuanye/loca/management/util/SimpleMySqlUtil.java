package com.chuanye.loca.management.util;

import java.util.List;

public class SimpleMySqlUtil {

    public static String limit(RequestPageBase page) {
        int startRow = page.getPageSize() * (page.getPageIndex() - 1);
        String format = " limit %s,%s";
        return String.format(format, startRow, page.getPageSize());
    }

    public static String orderByDescending(String key) {
        String format = " order by %s desc ";
        return String.format(format, key);
    }

    public static String orderBy(String key) {
        String format = " order by %s asc ";
        return String.format(format, key);
    }

    public static String OrderBySql(RequestPageBase page,String key){
        String format= page.getOrderByPattern() == 0 ? SimpleMySqlUtil.orderByDescending(key) : SimpleMySqlUtil.orderBy(key);
        return format;
    }

    public static String CountSql(String sql) {
        String result = new String();
        sql = sql.toLowerCase();
        int start = sql.indexOf("from");
        int end = -1;
        end = sql.indexOf("order");
        if (end == -1)
        {
            end = sql.indexOf("limit");
        }
        if (end == -1)
        {
            result = sql.substring(start);
        }
        else
        {
            result = sql.substring(start, end - start - 1);
        }
        return String.format(" select count(1) as count %s ", result);
    }

    public static String TimeSql(RequestPageBase page,String columnName) throws Exception {
        String sql = new String();
        if (page.getStartTime()!=null)
        {
            sql += " and %s >= '%s'";
            String stringStartTime=DateTimeUtil.getStringDate(page.getStartTime());
            sql = String.format(sql,columnName ,stringStartTime);
        }
        if (page.getEndTime()!=null)
        {
            sql += " and %s <= '%s'";
            String stringEndTime=DateTimeUtil.getStringDate(page.getEndTime());
            sql = String.format(sql,columnName ,stringEndTime);
        }
        return sql;
    }

    public static String KeywordSql(RequestPageBase page, String columnName) {
        String sql = new String();
        if (!StringUtil.isNullOrEmpty(page.getKeyword()))
        {
            sql += " and `%s` like '%"+page.getKeyword()+"%'";
            sql = String.format(sql, columnName);
        }
        return sql;
    }

    /**
     * 关键字匹配
     * @param value
     * @param columnName
     * @param andOr 0.and 1.or
     * @return
     */
    public static String KeywordSqlAndOr(String value, String columnName,int andOr) {
        String sql = new String();
        if (!StringUtil.isNullOrEmpty(value))
        {
            if (andOr == 0)
            {
                sql += " and ";
            }
            else
            {
                sql += " or ";
            }
            sql += "  `%s` like '%"+value+"%'";
            sql = String.format(sql, columnName);
        }
        return sql;
    }

    public static String in(List<Long> values,String columnName){
        String sql = new String();
        for (Object i:values){
            sql +="'"+i.toString()+"',";
        }
        if(values!=null && values.size()>0)
        {
            sql=sql.substring(0, sql.length()-1);
            sql=String.format(" and  `%s` in (%s)",columnName,sql);
            return sql;
        }
        return "";
    }


}
