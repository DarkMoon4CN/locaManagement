package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.model.Locapowermode;

import java.util.List;

public interface LocapowermodeDao {
    List<Locapowermode> get(long powerMode) throws Exception;
}
