package com.chuanye.loca.management.controller;

import com.chuanye.loca.management.model.Locatype;
import com.chuanye.loca.management.request.LocatypeGetRequest;
import com.chuanye.loca.management.service.LocatypeService;
import com.chuanye.loca.management.util.GuidUtil;
import com.chuanye.loca.management.util.ResultDtoBase;
import com.chuanye.loca.management.util.ResultFirstDto;
import com.chuanye.loca.management.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "locatype",description = "站点类型",tags = {"LocaType"})
@RestController
@RequestMapping("/locatype")
public class LocaTypeController {
    @Autowired
    private LocatypeService service;


    @ApiOperation(value = "获取单一")
    @PostMapping("/get")
    public ResultFirstDto<Locatype> getByLocaTypeId(@RequestBody LocatypeGetRequest obj) {
        ResultFirstDto<Locatype> result=new ResultFirstDto<Locatype>();
        try {
            List<Locatype> infos=service.get(obj.getLocaTypeId(),null);
            result.setFirstParam(infos!=null &&infos.size()>0?infos.get(0):null);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locatype/get Server Error");
        }
        return result;
    }

    @ApiOperation(value = "增加")
    @PostMapping("/add")
    public ResultDtoBase add(@RequestBody Locatype obj) {
        ResultDtoBase result=new ResultDtoBase();
        try {
            if(StringUtil.isNullOrEmpty(obj.getLocaTypeName()))
            {
                result.setStatus(-1);
                result.setMessage("LocaTypeName is null");
            }

            obj.setLocaTypeId(GuidUtil.newGuid());
            int status=service.add(obj);
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locatype/add Server Error");
        }
        return result;
    }

    @ApiOperation(value = "编辑")
    @PostMapping("/modify")
    public  ResultDtoBase modify(@RequestBody Locatype obj) {
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.modify(obj);
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locatype/modify Server Error");
        }
        return result;
    }

    @ApiOperation(value = "删除")
    @PostMapping("/remove")
    public  ResultDtoBase remove(@RequestBody LocatypeGetRequest obj){
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.remove(obj.getLocaTypeId());
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locatype/remove Server Error");
        }
        return result;
    }

    @ApiOperation(value = "获取所有")
    @PostMapping("/getall")
    public ResultFirstDto<List<Locatype>> getAll() {
        ResultFirstDto<List<Locatype>> result=new ResultFirstDto<List<Locatype>>();
        try {
            List<Locatype> infos=service.get(null,null);
            result.setFirstParam(infos);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locatype/getAll Server Error");
        }
        return result;
    }


}
