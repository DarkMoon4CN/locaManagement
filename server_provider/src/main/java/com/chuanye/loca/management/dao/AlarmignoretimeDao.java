package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.model.Alarmignoretime;
import com.chuanye.loca.management.response.AlarmignoretimeModel;

import java.rmi.server.ExportException;
import java.util.List;

public interface AlarmignoretimeDao {
    int add(Alarmignoretime info) throws Exception;
    List<AlarmignoretimeModel> get()throws Exception;
}
