package com.chuanye.loca.management.model;

public class Videoalarmtype {

  private long alarmType;
  private String alarmTypeName;


  public long getAlarmType() {
    return alarmType;
  }

  public void setAlarmType(long alarmType) {
    this.alarmType = alarmType;
  }


  public String getAlarmTypeName() {
    return alarmTypeName;
  }

  public void setAlarmTypeName(String alarmTypeName) {
    this.alarmTypeName = alarmTypeName;
  }

}
