package com.chuanye.loca.management.controller;

import com.chuanye.loca.management.model.Sysinfo;
import com.chuanye.loca.management.request.SysinfoGetRequest;
import com.chuanye.loca.management.service.SysinfoService;
import com.chuanye.loca.management.util.GuidUtil;
import com.chuanye.loca.management.util.ResultDtoBase;
import com.chuanye.loca.management.util.ResultFirstDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "sysinfo",description = "系统类型",tags = {"SysInfo"})
@RestController
@RequestMapping("/sysinfo")
public class SysInfoController {

    @Autowired
    private SysinfoService service;

    @ApiOperation(value = "获取单一")
    @PostMapping("/get")
    public ResultFirstDto<Sysinfo> getBySysId(@RequestBody SysinfoGetRequest obj) {
        ResultFirstDto<Sysinfo> result=new ResultFirstDto<Sysinfo>();
        try {
           List<Sysinfo> infos=service.get(obj.getSysId(),null);
           result.setFirstParam(infos!=null &&infos.size()>0?infos.get(0):null);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/sysinfo/get Server Error");
        }
        return result;
    }

    @ApiOperation(value = "增加")
    @PostMapping("/add")
    public ResultDtoBase add(@RequestBody Sysinfo obj) {
        ResultDtoBase result=new ResultDtoBase();
        try {
            obj.setSysId(GuidUtil.newGuid());
            int status=service.add(obj);
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/sysinfo/add Server Error");
        }
        return result;
    }

    @ApiOperation(value = "编辑")
    @PostMapping("/modify")
    public  ResultDtoBase modify(@RequestBody Sysinfo obj) {
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.modify(obj);
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/sysinfo/modify Server Error");
        }
        return result;
    }

    @ApiOperation(value = "删除")
    @PostMapping("/remove")
    public  ResultDtoBase remove(@RequestBody SysinfoGetRequest obj){
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.remove(obj.getSysId());
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/sysinfo/remove Server Error");
        }
        return result;
    }

    @ApiOperation(value = "获取全部")
    @PostMapping("/getall")
    public ResultFirstDto<List<Sysinfo>> getAll() {
        ResultFirstDto<List<Sysinfo>> result=new ResultFirstDto<List<Sysinfo>>();
        try {
            List<Sysinfo> infos=service.get(null,null);
            result.setFirstParam(infos);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/sysinfo/getall Server Error");
        }
        return result;
    }
}
