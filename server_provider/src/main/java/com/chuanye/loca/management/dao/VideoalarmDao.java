package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.model.Videoalarm;
import com.chuanye.loca.management.response.VideoalarmModel;
import com.chuanye.loca.management.util.RequestPageBase;

import java.util.List;

public interface VideoalarmDao {
    int getAlarmCountBynUnits(List<Long> nUnits)throws Exception;

    int add(Videoalarm info)throws Exception;

    int modify(Videoalarm info)throws  Exception;

    int remove(String alarmId)throws  Exception;

    VideoalarmModel get(String alarmId)throws Exception;

    List<VideoalarmModel> getByPage(String alarmId, String locaId, String addressCounty, String addressTown, String addressVillage, RequestPageBase page) throws Exception;

    int changeAuditState(String alarmId,int auditState)throws Exception;
}
