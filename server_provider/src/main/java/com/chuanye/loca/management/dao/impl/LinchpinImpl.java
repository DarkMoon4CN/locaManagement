package com.chuanye.loca.management.dao.impl;

import com.chuanye.loca.management.dao.JdbcTemplateBase;
import com.chuanye.loca.management.dao.LinchpinDao;
import com.chuanye.loca.management.model.Linchpin;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Primary
class LinchpinImpl extends JdbcTemplateBase implements LinchpinDao {
    public Linchpin get(long linchpinId) throws Exception {
        StringBuilder sql=new  StringBuilder() ;
        sql.append(" select * from linchpin  where 1=1 ");
        if(linchpinId > 0)
        {
            sql.append(" and `linchpinID`='"+linchpinId+"'");
        }
        List<Linchpin> linchpins = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<Linchpin>(Linchpin.class));
        return linchpins!=null && linchpins.size()>0?linchpins.get(0):null;
    }

    public List<Linchpin> sub(long parentId) throws Exception {
        StringBuilder sql=new  StringBuilder() ;
        sql.append(" select * from linchpin  where 1=1");
        if(parentId > 0)
        {
            sql.append(" and `ParentID`='"+parentId+"'");
        }
        List<Linchpin> linchpins = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<Linchpin>(Linchpin.class));
        return linchpins;
    }
}
