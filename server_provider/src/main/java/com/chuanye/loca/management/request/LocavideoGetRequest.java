package com.chuanye.loca.management.request;

public class LocavideoGetRequest {
    private String videoId;

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
