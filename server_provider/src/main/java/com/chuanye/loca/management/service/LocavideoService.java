package com.chuanye.loca.management.service;

import com.chuanye.loca.management.dao.LocavideoDao;
import com.chuanye.loca.management.model.Locavideo;
import com.chuanye.loca.management.response.LocavideoModel;
import com.chuanye.loca.management.util.RequestPageBase;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LocavideoService {
    @Resource
    private LocavideoDao dao;

    public int add(Locavideo info)throws Exception{
        return dao.add(info);
    }
    public int modify(Locavideo info) throws Exception{
        return dao.modify(info);
    }
    public int remove(String videoId)throws  Exception{
        return dao.remove(videoId);
    }
    public LocavideoModel get(String videoId)throws Exception{
        return dao.get(videoId);
    }
    public List<LocavideoModel> getByPage(String videoId, String locaId, RequestPageBase page)throws  Exception{
        return dao.getByPage(videoId,locaId,page);
    }
}
