package com.chuanye.loca.management.service;

import com.chuanye.loca.management.dao.LocasysDao;
import com.chuanye.loca.management.model.Locasys;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LocasysService {

    @Resource
    private LocasysDao dao;

    public int add(Locasys info)throws Exception{
            return dao.add(info);
    }
    public int remove(String locaSysId)throws Exception{
        return dao.remove(locaSysId);
    }
    public int remove(String locaId,String sysId)throws Exception{
        return dao.remove(locaId, sysId);
    }
    public List<Locasys> get(String locaId, String sysId) throws Exception {
        return dao.get(locaId, sysId);
    }
}
