package com.chuanye.loca.management.service;

import com.chuanye.loca.management.dao.LocalinkDao;
import com.chuanye.loca.management.model.Localink;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LocalinkService {
    @Resource
    private LocalinkDao dao;
    public List<Localink> get(long link) throws Exception{
        return dao.get(link);
    }
}
