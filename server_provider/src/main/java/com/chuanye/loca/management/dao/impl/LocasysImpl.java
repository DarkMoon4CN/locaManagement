package com.chuanye.loca.management.dao.impl;

import com.chuanye.loca.management.dao.JdbcTemplateBase;
import com.chuanye.loca.management.dao.LocasysDao;
import com.chuanye.loca.management.model.Locasys;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Primary
class LocasysImpl extends JdbcTemplateBase implements LocasysDao {
    public int add(Locasys info) throws Exception {
        String sql=insertSql("locasys",info);
        return update(sql);
    }

    public int remove(String locaSysId) throws Exception {
        Map<String,String> map=new HashMap<String, String>();
        map.put("LocaSysID",locaSysId);
        String sql=deleteSql("locasys",map);
        return update(sql);
    }

    public int remove(String locaId, String sysId) throws Exception {
        Map<String,String> map=new HashMap<String, String>();
        map.put("LocaID",locaId);
        map.put("SysID",sysId);
        String sql=deleteSql("locasys",map);
        return update(sql);
    }

    public List<Locasys> get(String locaId, String sysId) throws Exception {
        StringBuilder sql=new  StringBuilder() ;
        sql.append(" select * from locasys  where 1=1");
        if(locaId != null && locaId.length() != 0)
        {
            sql.append(" and LocaID='"+locaId+"'");
        }
        if(sysId != null && sysId.length() != 0)
        {
            sql.append(" and SysID='"+sysId+"'");
        }
        List<Locasys> sysInfos = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<Locasys>(Locasys.class));
        return sysInfos;
    }
}
