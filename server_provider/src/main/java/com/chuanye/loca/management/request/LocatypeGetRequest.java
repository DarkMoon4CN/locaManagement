package com.chuanye.loca.management.request;

public class LocatypeGetRequest {

    public String getLocaTypeId() {
        return LocaTypeId;
    }

    public void setLocaTypeId(String locaTypeId) {
        LocaTypeId = locaTypeId;
    }

    private String LocaTypeId;
}
