package com.chuanye.loca.management.controller;


import com.chuanye.loca.management.model.Locasys;
import com.chuanye.loca.management.request.LocasysRemoveByIdRequest;
import com.chuanye.loca.management.request.LocasysRemoveRequest;
import com.chuanye.loca.management.service.LocasysService;
import com.chuanye.loca.management.util.GuidUtil;
import com.chuanye.loca.management.util.ResultDtoBase;
import com.chuanye.loca.management.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.junit.validator.PublicClassValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "locasys",description = "站点系统关系",tags = {"LocaSys"})
@RestController
@RequestMapping("/locasys")
public class LocaSysController {
    @Autowired
    private LocasysService service;

    @ApiOperation(value = "增加")
    @PostMapping("/add")
    public ResultDtoBase add(@RequestBody Locasys obj) {
        ResultDtoBase result = new ResultDtoBase();
        try {
            if (StringUtil.isNullOrEmpty(obj.getLocaId()) || StringUtil.isNullOrEmpty(obj.getSysId())) {
                result.setStatus(-2);
                result.setMessage("LocaID or SysID is null");
                return result;
            }
            List<Locasys> locasys= service.get(obj.getLocaId(),obj.getSysId());
            if(locasys!=null && locasys.size() >0)
            {
                result.setStatus(-1);
                result.setMessage("LocaID and SysID is exist");
                return result;
            }
            obj.setLocaSysId(GuidUtil.newGuid());
            int status = service.add(obj);
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locasys/add Server Error");
        }
        return result;
    }

    @ApiOperation(value = "删除 根据LocaSysID")
    @PostMapping("/removebyid")
    public  ResultDtoBase removeById(@RequestBody LocasysRemoveByIdRequest obj){
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.remove(obj.getLocaSysId());
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locasys/removebyid Server Error");
        }
        return result;
    }

    @ApiOperation(value = "删除 根据LocaID和SysID")
    @PostMapping("/remove")
    public  ResultDtoBase removeById(@RequestBody LocasysRemoveRequest obj){
        ResultDtoBase result=new ResultDtoBase();
        try {
            int status=service.remove(obj.getLocaId(),obj.getSysId());
            result.setStatus(status);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locasys/remove Server Error");
        }
        return result;
    }


}
