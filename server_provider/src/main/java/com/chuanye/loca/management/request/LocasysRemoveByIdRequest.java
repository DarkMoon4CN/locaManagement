package com.chuanye.loca.management.request;

public class LocasysRemoveByIdRequest {
    private  String locaSysId;

    public String getLocaSysId() {
        return locaSysId;
    }

    public void setLocaSysId(String locaSysId) {
        this.locaSysId = locaSysId;
    }
}
