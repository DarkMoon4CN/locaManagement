package com.chuanye.loca.management.response;

import com.chuanye.loca.management.model.Locavideo;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.util.List;

public class LocaInfoModel {

    private String locaId;
    private String locaName;
    private String locaTypeId;
    private double locaHigh;

    public String getLocaId() {
        return locaId;
    }

    public void setLocaId(String locaId) {
        this.locaId = locaId;
    }

    public String getLocaName() {
        return locaName;
    }

    public void setLocaName(String locaName) {
        this.locaName = locaName;
    }

    public String getLocaTypeId() {
        return locaTypeId;
    }

    public void setLocaTypeId(String locaTypeId) {
        this.locaTypeId = locaTypeId;
    }

    public double getLocaHigh() {
        return locaHigh;
    }

    public void setLocaHigh(double locaHigh) {
        this.locaHigh = locaHigh;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public int getLink() {
        return link;
    }

    public void setLink(int link) {
        this.link = link;
    }

    public int getPowerMode() {
        return powerMode;
    }

    public void setPowerMode(int powerMode) {
        this.powerMode = powerMode;
    }

    public String getAddressCounty() {
        return addressCounty;
    }

    public void setAddressCounty(String addressCounty) {
        this.addressCounty = addressCounty;
    }

    public String getAddressCountyName() {
        return addressCountyName;
    }

    public void setAddressCountyName(String addressCountyName) {
        this.addressCountyName = addressCountyName;
    }

    public String getAddressTown() {
        return addressTown;
    }

    public void setAddressTown(String addressTown) {
        this.addressTown = addressTown;
    }

    public String getAddressTownName() {
        return addressTownName;
    }

    public void setAddressTownName(String addressTownName) {
        this.addressTownName = addressTownName;
    }

    public String getAddressVillage() {
        return addressVillage;
    }

    public void setAddressVillage(String addressVillage) {
        this.addressVillage = addressVillage;
    }

    public String getAddressVillageName() {
        return addressVillageName;
    }

    public void setAddressVillageName(String addressVillageName) {
        this.addressVillageName = addressVillageName;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8",
            shape = JsonFormat.Shape.STRING,
            locale ="zh"
    )
    @ApiModelProperty(value = "告警时间",example = "yyyy-MM-dd HH:mm:ss")
    public Timestamp getAddTime() {
        return addTime;
    }

    public void setAddTime(Timestamp addTime) {
        this.addTime = addTime;
    }

    public int getAlarmCount() {
        return alarmCount;
    }

    public void setAlarmCount(int alarmCount) {
        this.alarmCount = alarmCount;
    }

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8",
            shape = JsonFormat.Shape.STRING,
            locale ="zh"
    )
    @ApiModelProperty(value = "告警时间",example = "yyyy-MM-dd HH:mm:ss")
    public Timestamp getAlarmNextTime() {
        return alarmNextTime;
    }

    public void setAlarmNextTime(Timestamp alarmNextTime) {
        this.alarmNextTime = alarmNextTime;
    }

    private String describe;
    private int link;
    private int powerMode;
    private String addressCounty;
    private String addressCountyName;
    private String addressTown;
    private String addressTownName;
    private String addressVillage;
    private String addressVillageName;
    private double longitude;
    private double latitude;
    private double altitude;
    private java.sql.Timestamp addTime;
    private int alarmCount;
    private java.sql.Timestamp alarmNextTime;

    public String getLinkName() {
        return LinkName;
    }

    public void setLinkName(String linkName) {
        LinkName = linkName;
    }

    public String getPowerModeName() {
        return PowerModeName;
    }

    public void setPowerModeName(String powerModeName) {
        PowerModeName = powerModeName;
    }

    private String LinkName;
    private String PowerModeName;




    public List<Locavideo> getVideos() {
        return videos;
    }

    public void setVideos(List<Locavideo> videos) {
        this.videos = videos;
    }

    private List<Locavideo> videos;
}
