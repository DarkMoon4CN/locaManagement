package com.chuanye.loca.management.request;

public class LocapowermodeRequest {
    private  long powerMode;

    public long getPowerMode() {
        return powerMode;
    }

    public void setPowerMode(long powerMode) {
        this.powerMode = powerMode;
    }
}
