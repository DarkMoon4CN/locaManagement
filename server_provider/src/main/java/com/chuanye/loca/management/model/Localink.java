package com.chuanye.loca.management.model;


public class Localink {

  private long link;
  private String linkName;

  public long getLink() {
    return link;
  }

  public void setLink(long link) {
    this.link = link;
  }


  public String getLinkName() {
    return linkName;
  }

  public void setLinkName(String linkName) {
    this.linkName = linkName;
  }
}
