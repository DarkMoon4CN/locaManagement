package com.chuanye.loca.management.model;


public class Linchpin {

  private long linchpinId;
  private String linchpinName;
  private long parentId;


  public long getLinchpinId() {
    return linchpinId;
  }

  public void setLinchpinId(long linchpinId) {
    this.linchpinId = linchpinId;
  }

  public String getLinchpinName() {
    return linchpinName;
  }

  public void setLinchpinName(String linchpinName) {
    this.linchpinName = linchpinName;
  }

  public long getParentId() {
    return parentId;
  }

  public void setParentId(long parentId) {
    this.parentId = parentId;
  }

}
