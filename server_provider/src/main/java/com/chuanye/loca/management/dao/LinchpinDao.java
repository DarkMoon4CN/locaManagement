package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.model.Linchpin;

import java.util.List;

public interface LinchpinDao {
    Linchpin get(long linchpinId) throws Exception;
    List<Linchpin> sub(long parentId) throws Exception;
}
