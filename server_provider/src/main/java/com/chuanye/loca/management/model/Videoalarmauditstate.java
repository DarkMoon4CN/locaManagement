package com.chuanye.loca.management.model;

public class Videoalarmauditstate {

  private long auditState;
  private String auditStateName;


  public long getAuditState() {
    return auditState;
  }

  public void setAuditState(long auditState) {
    this.auditState = auditState;
  }

  public String getAuditStateName() {
    return auditStateName;
  }

  public void setAuditStateName(String auditStateName) {
    this.auditStateName = auditStateName;
  }
}
