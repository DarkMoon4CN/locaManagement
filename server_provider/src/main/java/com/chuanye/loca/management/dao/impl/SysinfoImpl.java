package com.chuanye.loca.management.dao.impl;

import com.chuanye.loca.management.dao.JdbcTemplateBase;
import com.chuanye.loca.management.dao.SysinfoDao;
import com.chuanye.loca.management.model.Sysinfo;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Primary
class SysinfoImpl extends JdbcTemplateBase implements SysinfoDao {
    public List<Sysinfo> get(String sysId, String sysName) throws Exception {
        StringBuilder sql=new  StringBuilder() ;
        sql.append(" select * from sysinfo  where 1=1");
        if(sysId != null && sysId.length() != 0)
        {
            sql.append(" and SysID='"+sysId+"'");
        }
        if(sysName != null && sysName.length() != 0)
        {
            sql.append(" and SysName='"+sysName+"'");
        }
        sql.append(" order by AddTime desc ");
        List<Sysinfo> sysInfos = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<Sysinfo>(Sysinfo.class));
        return sysInfos;
    }
    public int add(Sysinfo info) throws  Exception {
        String sql=insertSql("sysInfo",info);
        return update(sql);
    }
    public int modify(Sysinfo info) throws Exception {
        String sql=updateSql("sysInfo",info,new String[]{"SysID"});
        return update(sql);
    }
    public int remove(String sysID) throws Exception {
        Map<String,String> map=new HashMap<String, String>();
        map.put("SysID",sysID);
        String sql=deleteSql("sysInfo",map);
        return update(sql);
    }
}
