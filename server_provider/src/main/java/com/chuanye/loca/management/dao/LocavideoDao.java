package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.model.Locavideo;
import com.chuanye.loca.management.response.LocavideoModel;
import com.chuanye.loca.management.util.RequestPageBase;

import java.util.List;

public interface LocavideoDao {
       int add(Locavideo info)throws Exception;
       int modify(Locavideo info) throws Exception;
       int remove(String videoId)throws  Exception;
       LocavideoModel get(String videoId)throws Exception;
       List<LocavideoModel> getByPage(String videoId, String locaId, RequestPageBase page)throws  Exception;
}
