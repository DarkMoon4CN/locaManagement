package com.chuanye.loca.management.util;

public class Pickout {
    public static RequestPageBase InitPageBase(RequestPageBase entity)
    {
        RequestPageBase page = new RequestPageBase();
        if (entity != null)
        {
            page.setPageIndex(entity.getPageIndex() == 0 ? 1 : entity.getPageIndex());
            page.setPageSize(entity.getPageSize() == 0 ? 20 : entity.getPageSize());
            page.setStartTime(entity.getStartTime());
            page.setEndTime(entity.getEndTime());
            page.setKeyword(entity.getKeyword());
            page.setOrderByPattern(entity.getOrderByPattern());
            page.setOrderByKey(entity.getOrderByKey());
        }
        return page;
    }
}
