package com.chuanye.loca.management.service;

import com.chuanye.loca.management.dao.VideoalarmauditstateDao;
import com.chuanye.loca.management.model.Videoalarmauditstate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VideoalarmauditstateService {
    @Resource
    private VideoalarmauditstateDao dao;

    public List<Videoalarmauditstate> get(long auditState) throws Exception {
        return dao.get(auditState);
    }
}
