package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.model.Locainfo;
import com.chuanye.loca.management.response.LocaInfoModel;
import com.chuanye.loca.management.response.LocainfoTreeModel;
import com.chuanye.loca.management.util.RequestPageBase;

import java.util.List;

public interface LocainfoDao {

     int add(Locainfo info)throws  Exception;
     int modify(Locainfo info) throws Exception;
     int remove(String locaId)throws  Exception;

     List<LocainfoTreeModel> getByTree(String addressCounty) throws Exception;
     List<LocaInfoModel>  getByPage(String locaId, String addressCounty, RequestPageBase page) throws Exception;
     LocaInfoModel get(String locaId)throws Exception;
}
