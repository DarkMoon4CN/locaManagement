package com.chuanye.loca.management.model;


public class Locapowermode {

  private long powerMode;
  private String powerModeName;


  public long getPowerMode() {
    return powerMode;
  }

  public void setPowerMode(long powerMode) {
    this.powerMode = powerMode;
  }


  public String getPowerModeName() {
    return powerModeName;
  }

  public void setPowerModeName(String powerModeName) {
    this.powerModeName = powerModeName;
  }

}
