package com.chuanye.loca.management;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = "com.chuanye.loca.management.dao")
public class Application_Provider {
    public static void main(String[] args){
        SpringApplication.run(Application_Provider.class);
    }
}