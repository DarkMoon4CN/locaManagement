package com.chuanye.loca.management.service;

import com.chuanye.loca.management.dao.LocainfoDao;
import com.chuanye.loca.management.model.Locainfo;
import com.chuanye.loca.management.response.LocaInfoModel;
import com.chuanye.loca.management.response.LocainfoTreeModel;
import com.chuanye.loca.management.util.RequestPageBase;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LocainfoService {
    @Resource
    private LocainfoDao dao;
    public List<LocainfoTreeModel> getByTree(String addressCounty) throws Exception{
        return dao.getByTree(addressCounty);
    }
    public int add(Locainfo info)throws  Exception{
        return dao.add(info);
    }
    public int modify(Locainfo info) throws Exception{
        return dao.modify(info);
    }
    public int remove(String locaId)throws  Exception{
        return dao.remove(locaId);
    }
    public List<LocaInfoModel>  getByPage(String locaId, String addressCounty, RequestPageBase page) throws Exception{
        return dao.getByPage(locaId,addressCounty,page);
    }
    public LocaInfoModel get(String locaId)throws Exception{
        return dao.get(locaId);
    }
}
