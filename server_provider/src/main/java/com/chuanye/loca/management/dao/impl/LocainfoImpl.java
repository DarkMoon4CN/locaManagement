package com.chuanye.loca.management.dao.impl;

import com.chuanye.loca.management.dao.JdbcTemplateBase;
import com.chuanye.loca.management.dao.LocainfoDao;
import com.chuanye.loca.management.model.Locainfo;
import com.chuanye.loca.management.model.Locavideo;
import com.chuanye.loca.management.response.LocaInfoModel;
import com.chuanye.loca.management.response.LocainfoChildTreeModel;
import com.chuanye.loca.management.response.LocainfoTreeModel;
import com.chuanye.loca.management.util.RequestPageBase;
import com.chuanye.loca.management.util.SimpleMySqlUtil;
import com.chuanye.loca.management.util.StringUtil;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
@Primary
class LocainfoImpl extends JdbcTemplateBase implements LocainfoDao {
    public int add(Locainfo info)throws  Exception {
       String sql=insertSql("locainfo",info);
       return update(sql);
    }

    public int modify(Locainfo info) throws Exception {
        String sql=updateSql("locainfo",info,new String[]{"LocaId"});
        return update(sql);
    }

    public int remove(String locaId) throws Exception {
        Map<String,String> map=new HashMap<String, String>();
        map.put("LocaID",locaId);
        String sql=deleteSql("Locainfo",map);
        return update(sql);
    }

    public List<LocainfoTreeModel> getByTree(String addressCounty) throws Exception {
        String sql=new String();
        sql += " select a.*,b.LocaTypeName,b.LocaImgPath,c.LinchpinName as AddressCountyName,d.LinchpinName as AddressTownName,e.LinchpinName as AddressVillageName  ";
        sql += ",(DATE_ADD(f.IgnoreTime,INTERVAL f.Value MINUTE)) as AlarmNextTime ";
        sql += " from locainfo as a ";
        sql += " left join locatype as b on a.LocaTypeID = b.LocaTypeID ";
        sql += " left join linchpin as c on a.AddressCounty=c.LinchpinID ";
        sql += " left join linchpin as d on a.AddressTown=d.LinchpinID ";
        sql += " left join linchpin as e on a.AddressVillage=e.LinchpinID";
        sql += " left join alarmignoretime as f on a.LocaID =f.LocaID ";
        sql += " where 1=1 ";
        if (!StringUtil.isNullOrEmpty(addressCounty))
        {
            sql += " and a.addressCounty = '%s'";
            sql = String.format(sql, addressCounty);
        }
        List<LocaInfoModel> locaInfoModels = jdbcTemplate.query(sql, new BeanPropertyRowMapper<LocaInfoModel>(LocaInfoModel.class));
        for (LocaInfoModel locaInfoModel:locaInfoModels){
            List<Locavideo> locavideos= getlocavideo(locaInfoModel.getLocaId());
            locaInfoModel.setVideos(locavideos);
            if(locavideos!=null && locavideos.size()>0){
                List<Long> nUnits=locavideos.stream().map(Locavideo::getNUnit).collect(Collectors.toList());
                int count= getAlarmCountBynUnits(nUnits);
                locaInfoModel.setAlarmCount(count);
            }
        }

        List<LocainfoTreeModel> models = new ArrayList<LocainfoTreeModel>();
        Map<String,String>  addressCountyIDs=new HashMap<String, String>();
        for (LocaInfoModel countyModel :locaInfoModels){
            if(addressCountyIDs.get(countyModel.getAddressCounty())==null){
                addressCountyIDs.put(countyModel.getAddressCounty(),countyModel.getAddressCountyName());
            }
        }
        for (Map.Entry<String,String> map:addressCountyIDs.entrySet()) {
            LocainfoTreeModel model = new LocainfoTreeModel();
            model.setNodeId(map.getKey());
            model.setNodeName(map.getValue());

            List<LocaInfoModel> countySubs = new ArrayList<LocaInfoModel>();
            for (LocaInfoModel countyTwonModel : locaInfoModels) {
                if (map.getKey().equals(countyTwonModel.getAddressCounty())) {
                    countySubs.add(countyTwonModel);
                }
            }
            Map<String,String>  addressTownIDs=new HashMap<String, String>();
            for (LocaInfoModel twonModel:countySubs){
                if(addressTownIDs.get(twonModel.getAddressTown())==null){
                    addressTownIDs.put(twonModel.getAddressTown(),twonModel.getAddressTownName());
                }
            }

            List<LocainfoChildTreeModel> subModels=new ArrayList<LocainfoChildTreeModel>();
            for (Map.Entry<String,String> map2:addressTownIDs.entrySet()){
                LocainfoChildTreeModel subModel=new LocainfoChildTreeModel();
                subModel.setNodeId(map2.getKey());
                subModel.setNodeName(map2.getValue());
                List<LocaInfoModel> countyTownSubs=locaInfoModels.stream()
                        .filter(p->p.getAddressCounty().equals(map.getKey()) && p.getAddressTown().equals(map2.getKey()))
                        .collect(Collectors.toList());
                subModel.setList(countyTownSubs);
                subModels.add(subModel);
            }
            model.setChild(subModels);
            models.add(model);
        }
        return models;
    }

    public List<LocaInfoModel> getByPage(String locaId, String addressCounty, RequestPageBase page) throws Exception {
        String sql=new String();
        sql += " select a.*,b.LocaTypeName,b.LocaImgPath,c.LinchpinName as AddressCountyName,d.LinchpinName as AddressTownName,e.LinchpinName as AddressVillageName  ";
        sql +="  ,g.LinkName,h.PowerModeName  ";
        sql += " ,(DATE_ADD(f.IgnoreTime,INTERVAL f.Value MINUTE)) as AlarmNextTime ";
        sql += " from locainfo as a ";
        sql += " left join locatype as b on a.LocaTypeID = b.LocaTypeID ";
        sql += " left join linchpin as c on a.AddressCounty=c.LinchpinID ";
        sql += " left join linchpin as d on a.AddressTown=d.LinchpinID ";
        sql += " left join linchpin as e on a.AddressVillage=e.LinchpinID";
        sql += " left join alarmignoretime as f on a.LocaID =f.LocaID ";
        sql += " left join localink as g on a.Link =g.Link ";
        sql += " left join locapowermode as h on a.PowerMode =h.PowerMode ";
        sql += " where 1=1 ";
        if (!StringUtil.isNullOrEmpty(locaId))
        {
            sql += " and a.`LocaID` = '%s'";
            sql = String.format(sql, locaId);
        }
        if (!StringUtil.isNullOrEmpty(addressCounty))
        {
            sql += " and a.`addressCounty` = '%s'";
            sql = String.format(sql, addressCounty);
        }
        if(page!=null)
        {
            if (!StringUtil.isNullOrEmpty(page.getKeyword())) {
                String paramsSql=new String();
                paramsSql += " a.`LocaID` like  '%"+page.getKeyword()+"%'";
                paramsSql += " or a.`LocaName` like '%"+page.getKeyword()+"%'";
                paramsSql += " or b.`LocaTypeName` like '%"+page.getKeyword()+"%' ";
                sql +=String.format(" and (%s) ",paramsSql);
            }
            String timeSql = SimpleMySqlUtil.TimeSql(page," a.`AddTime` ") ;
            sql +=timeSql;
            String  countSql = SimpleMySqlUtil.CountSql(sql);
            int count=jdbcTemplate.queryForObject(countSql, Integer.class);
            page.setTotalRows(count);
            sql += SimpleMySqlUtil.OrderBySql(page," a.`AddTime` ");
            sql +=SimpleMySqlUtil.limit(page);
        }
        List<LocaInfoModel> locaInfoModels = jdbcTemplate.query(sql, new BeanPropertyRowMapper<LocaInfoModel>(LocaInfoModel.class));
        for (LocaInfoModel locaInfoModel:locaInfoModels){
            List<Locavideo> locavideos= getlocavideo(locaInfoModel.getLocaId());
            locaInfoModel.setVideos(locavideos);
            if(locavideos!=null && locavideos.size()>0){
                List<Long> nUnits=locavideos.stream().map(Locavideo::getNUnit).collect(Collectors.toList());
                int count= getAlarmCountBynUnits(nUnits);
                locaInfoModel.setAlarmCount(count);
            }
        }
        return locaInfoModels;
    }

    public LocaInfoModel get(String locaId) throws Exception {
        List<LocaInfoModel> locaInfoModels=this.getByPage(locaId,null,null);
        return locaInfoModels!=null && locaInfoModels.size()>0?locaInfoModels.get(0):null;
    }

    private List<Locavideo> getlocavideo(String locaId) throws Exception {
        StringBuilder sql=new  StringBuilder() ;
        sql.append(" select * from locavideo  where 1=1");
        if(!StringUtil.isNullOrEmpty(locaId))
        {
            sql.append(" and `LocaID`='"+locaId+"'");
        }
        List<Locavideo> locavideos = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<Locavideo>(Locavideo.class));
        return locavideos;
    }

    private int getAlarmCountBynUnits(List<Long> nUnits) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select count(1) as `Count` from  videoalarm  where 1=1");
        sql.append(SimpleMySqlUtil.in(nUnits,"NUnit"));
        String countSql=sql.toString();
        int count = jdbcTemplate.queryForObject(sql.toString(), Integer.class);
        return count;
    }
}
