package com.chuanye.loca.management.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class LocainfoGetRequest {
    private  String locaId;

    @ApiModelProperty(value = "站点编号")
    public String getLocaId() {
        return locaId;
    }

    public void setLocaId(String locaId) {
        this.locaId = locaId;
    }
}
