package com.chuanye.loca.management.service;

import com.chuanye.loca.management.dao.LinchpinDao;
import com.chuanye.loca.management.model.Linchpin;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LinchpinService {
    @Resource
    private LinchpinDao dao;

    public Linchpin get(long linchpinId) throws Exception{
        return dao.get(linchpinId);
    }
    public List<Linchpin> sub(long parentId) throws Exception{
        return dao.sub(parentId);
    }
}
