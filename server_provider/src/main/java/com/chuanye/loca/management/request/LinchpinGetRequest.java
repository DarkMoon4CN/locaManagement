package com.chuanye.loca.management.request;

public class LinchpinGetRequest {
    public long getLinchpinId() {
        return linchpinId;
    }
    public void setLinchpinId(long linchpinId) {
        this.linchpinId = linchpinId;
    }
    private long linchpinId;
}
