package com.chuanye.loca.management.controller;


import com.chuanye.loca.management.model.Videoalarmauditstate;
import com.chuanye.loca.management.service.VideoalarmauditstateService;
import com.chuanye.loca.management.util.ResultFirstDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "videoalarmauditstate",description = "视频告警审核状态",tags = {"VideoAlarmAuditState"})
@RestController
@RequestMapping("/videoalarmauditstate")
public class VideoAlarmAuditStateController {
    @Autowired
    private VideoalarmauditstateService service;

    @ApiOperation(value = "获取全部")
    @PostMapping("/getall")
    public ResultFirstDto<List<Videoalarmauditstate>> getAll() {
        ResultFirstDto<List<Videoalarmauditstate>> result=new ResultFirstDto<>();
        try {
            List<Videoalarmauditstate> infos=service.get(0);
            result.setFirstParam(infos);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/videoalarmauditstate/getall Server Error");
        }
        return result;
    }
}
