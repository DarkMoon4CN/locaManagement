package com.chuanye.loca.management.util;

public class ResultFirstDto<T1> extends ResultDtoBase
{
    private T1 firstParam;

    /**
     * @return the firstParam
     */
    public T1 getFirstParam() {
        return firstParam;
    }

    /**
     * @param firstParam the firstParam to data
     */
    public void setFirstParam(T1 firstParam) {
        this.firstParam = firstParam;
    }
}