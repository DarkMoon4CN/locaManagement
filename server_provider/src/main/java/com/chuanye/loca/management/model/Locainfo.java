package com.chuanye.loca.management.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;


public class Locainfo {

  private String locaId;
  private String locaName;
  private String locaTypeId;
  private double locaHigh;
  private String describe;
  private long link;
  private long powerMode;
  private String addressCounty;
  private String addressTown;
  private String addressVillage;
  private double longitude;
  private double latitude;
  private double altitude;
  private java.sql.Timestamp addTime;


  public String getLocaId() {
    return locaId;
  }

  public void setLocaId(String locaId) {
    this.locaId = locaId;
  }


  public String getLocaName() {
    return locaName;
  }

  public void setLocaName(String locaName) {
    this.locaName = locaName;
  }


  public String getLocaTypeId() {
    return locaTypeId;
  }

  public void setLocaTypeId(String locaTypeId) {
    this.locaTypeId = locaTypeId;
  }


  public double getLocaHigh() {
    return locaHigh;
  }

  public void setLocaHigh(double locaHigh) {
    this.locaHigh = locaHigh;
  }


  public String getDescribe() {
    return describe;
  }

  public void setDescribe(String describe) {
    this.describe = describe;
  }


  public long getLink() {
    return link;
  }

  public void setLink(long link) {
    this.link = link;
  }


  public long getPowerMode() {
    return powerMode;
  }

  public void setPowerMode(long powerMode) {
    this.powerMode = powerMode;
  }


  public String getAddressCounty() {
    return addressCounty;
  }

  public void setAddressCounty(String addressCounty) {
    this.addressCounty = addressCounty;
  }


  public String getAddressTown() {
    return addressTown;
  }

  public void setAddressTown(String addressTown) {
    this.addressTown = addressTown;
  }


  public String getAddressVillage() {
    return addressVillage;
  }

  public void setAddressVillage(String addressVillage) {
    this.addressVillage = addressVillage;
  }


  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }


  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }


  public double getAltitude() {
    return altitude;
  }

  public void setAltitude(double altitude) {
    this.altitude = altitude;
  }

  @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
  @JsonFormat(
          pattern = "yyyy-MM-dd HH:mm:ss",
          timezone = "GMT+8",
          shape = JsonFormat.Shape.STRING,
          locale ="zh"
  )
  @ApiModelProperty(value = "增加时间",example = "yyyy-MM-dd HH:mm:ss")
  public java.sql.Timestamp getAddTime() {
    return addTime;
  }

  public void setAddTime(java.sql.Timestamp addTime) {
    this.addTime = addTime;
  }

}
