package com.chuanye.loca.management.model;


public class Locavideo {

  private String videoId;
  private String locaId;
  private long index;
  private String videoName;
  private String videoPath;
  private String flvVideoPath;
  private String hlsVideoPath;
  private long nUnit;


  public String getVideoId() {
    return videoId;
  }

  public void setVideoId(String videoId) {
    this.videoId = videoId;
  }


  public String getLocaId() {
    return locaId;
  }

  public void setLocaId(String locaId) {
    this.locaId = locaId;
  }


  public long getIndex() {
    return index;
  }

  public void setIndex(long index) {
    this.index = index;
  }


  public String getVideoName() {
    return videoName;
  }

  public void setVideoName(String videoName) {
    this.videoName = videoName;
  }


  public String getVideoPath() {
    return videoPath;
  }

  public void setVideoPath(String videoPath) {
    this.videoPath = videoPath;
  }


  public String getFlvVideoPath() {
    return flvVideoPath;
  }

  public void setFlvVideoPath(String flvVideoPath) {
    this.flvVideoPath = flvVideoPath;
  }


  public String getHlsVideoPath() {
    return hlsVideoPath;
  }

  public void setHlsVideoPath(String hlsVideoPath) {
    this.hlsVideoPath = hlsVideoPath;
  }


  public long getNUnit() {
    return nUnit;
  }

  public void setNUnit(long nUnit) {
    this.nUnit = nUnit;
  }

}
