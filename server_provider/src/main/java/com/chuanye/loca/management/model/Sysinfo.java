package com.chuanye.loca.management.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@ApiModel(description = "请求体")
public class Sysinfo {

    @ApiModelProperty(value = "系统名称")
    public String getSysName() {
        return sysName;
    }

    public void setSysName(String sysName) {
        this.sysName = sysName;
    }

    @ApiModelProperty(value = "描述")
    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8",
            shape = JsonFormat.Shape.STRING,
            locale ="zh"
    )
    @ApiModelProperty(value = "增加时间",example = "yyyy-MM-dd HH:mm:ss")
    public java.sql.Timestamp getAddTime() {
        return addTime;
    }

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8",
            shape = JsonFormat.Shape.STRING,
            locale ="zh"
    )
    public void setAddTime(java.sql.Timestamp addTime) {
        this.addTime = addTime;
    }

    public String toString() {
        return "SysInfo{" +
                "SysId='" + sysId + '\'' +
                ", SysName='" + sysName + '\'' +
                ", Describe='" + describe + '\'' +
                ", AddTime=" + addTime +
                '}';
    }


    @ApiModelProperty( value = "主键")
    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    private String sysId;

    private String sysName;

    private String describe;

    private java.sql.Timestamp addTime;
}
