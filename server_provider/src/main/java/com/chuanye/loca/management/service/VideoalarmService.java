package com.chuanye.loca.management.service;

import com.chuanye.loca.management.dao.AlarmignoretimeDao;
import com.chuanye.loca.management.dao.VideoalarmDao;
import com.chuanye.loca.management.model.Alarmignoretime;
import com.chuanye.loca.management.model.Videoalarm;
import com.chuanye.loca.management.response.VideoalarmModel;
import com.chuanye.loca.management.util.RequestPageBase;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VideoalarmService {
    @Resource
    private VideoalarmDao dao;


    public int getAlarmCountBynUnits(List<Long> nUnits)throws Exception
    {
        return dao.getAlarmCountBynUnits(nUnits);
    }

    public int add(Videoalarm info)throws Exception{
        return dao.add(info);
    }

    public int modify(Videoalarm info)throws  Exception{
        return dao.modify(info);
    }

    public int remove(String alarmId)throws  Exception{
        return dao.remove(alarmId);
    }

    public VideoalarmModel get(String alarmId)throws Exception{
        return dao.get(alarmId);
    }

    public List<VideoalarmModel> getByPage(String alarmId, String locaId, String addressCounty, String addressTown, String addressVillage, RequestPageBase page) throws Exception{
        return dao.getByPage(alarmId,locaId,addressCounty,addressTown,addressVillage,page);
    }

    public int changeAuditState(String alarmId, int auditState) throws Exception {
        return dao.changeAuditState(alarmId, auditState);
    }

}
