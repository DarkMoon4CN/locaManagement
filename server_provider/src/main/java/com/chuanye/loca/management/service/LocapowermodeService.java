package com.chuanye.loca.management.service;

import com.chuanye.loca.management.dao.LocapowermodeDao;
import com.chuanye.loca.management.model.Locapowermode;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LocapowermodeService {
    @Resource
    private LocapowermodeDao dao;
    public List<Locapowermode> get(long powerMode) throws Exception{
        return dao.get(powerMode);
    }
}
