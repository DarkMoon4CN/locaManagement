package com.chuanye.loca.management.dao.impl;

import com.chuanye.loca.management.dao.JdbcTemplateBase;
import com.chuanye.loca.management.dao.VideoalarmtypeDao;
import com.chuanye.loca.management.model.Videoalarmtype;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Primary
class VideoalarmtypeImpl extends JdbcTemplateBase implements VideoalarmtypeDao {
    public List<Videoalarmtype> get(long alarmType) throws Exception {
        StringBuilder sql=new  StringBuilder() ;
        sql.append(" select * from videoalarmtype  where 1=1 ");
        if(alarmType > 0)
        {
            sql.append(" and `AlarmType`='"+alarmType+"'");
        }
        sql.append(" order by `AlarmType` asc ");
        List<Videoalarmtype> videoalarmtypes = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<Videoalarmtype>(Videoalarmtype.class));
        return videoalarmtypes;
    }
}
