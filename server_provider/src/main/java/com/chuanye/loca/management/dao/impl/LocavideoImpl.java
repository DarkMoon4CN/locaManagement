package com.chuanye.loca.management.dao.impl;

import com.chuanye.loca.management.dao.JdbcTemplateBase;
import com.chuanye.loca.management.dao.LocavideoDao;
import com.chuanye.loca.management.model.Locavideo;
import com.chuanye.loca.management.response.LocavideoModel;
import com.chuanye.loca.management.util.RequestPageBase;
import com.chuanye.loca.management.util.SimpleMySqlUtil;
import com.chuanye.loca.management.util.StringUtil;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Primary
class LocavideoImpl extends JdbcTemplateBase implements LocavideoDao {
    public int add(Locavideo info) throws Exception {
        String sql=insertSql("locavideo",info);
        return update(sql);
    }

    public int modify(Locavideo info) throws Exception {
        String sql=updateSql("locavideo",info,new String[]{"VideoId"});
        return update(sql);
    }

    public int remove(String videoId) throws Exception {
        Map<String,String> map=new HashMap<String, String>();
        map.put("VideoId",videoId);
        String sql=deleteSql("locavideo",map);
        return update(sql);
    }

    public LocavideoModel get(String videoId) throws Exception {
        List<LocavideoModel> locaVideoModels=getByPage(videoId,null,null);
        return locaVideoModels!=null && locaVideoModels.size()>0?locaVideoModels.get(0):null;
    }

    public List<LocavideoModel> getByPage(String videoId, String locaId, RequestPageBase page) throws Exception {
        String sql=new String();
        sql +=" select a.*,b.LocaName from locavideo as a left outer join locainfo as b  on a.LocaID =b.LocaID ";
        sql +=" where 1=1 ";
        if (!StringUtil.isNullOrEmpty(locaId))
        {
            sql += " and a.`LocaID` = '%s'";
            sql = String.format(sql, locaId);
        }
        if (!StringUtil.isNullOrEmpty(videoId))
        {
            sql += " and a.`VideoId` = '%s'";
            sql = String.format(sql, videoId);
        }

        if(page!=null)
        {
            if (!StringUtil.isNullOrEmpty(page.getKeyword())) {
                String paramsSql=new String();
                paramsSql += " a.`LocaID` like  '%"+page.getKeyword()+"%'";
                paramsSql += " or b.`LocaName` like '%"+page.getKeyword()+"%'";
                paramsSql += " or a.`VideoName` like '%"+page.getKeyword()+"%'";
                sql +=String.format(" and (%s) ",paramsSql);
            }
            String timeSql = SimpleMySqlUtil.TimeSql(page," a.`VideoID` ") ;
            sql +=timeSql;
            String  countSql = SimpleMySqlUtil.CountSql(sql);
            int count=jdbcTemplate.queryForObject(countSql, Integer.class);
            page.setTotalRows(count);
            sql+=SimpleMySqlUtil.limit(page);
        }

        List<LocavideoModel> locaVideoModels = jdbcTemplate.query(sql, new BeanPropertyRowMapper<LocavideoModel>(LocavideoModel.class));
        return locaVideoModels;
    }
}
