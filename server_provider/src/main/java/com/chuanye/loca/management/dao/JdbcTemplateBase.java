package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.util.DateTimeUtil;
import com.chuanye.loca.management.util.SimpleClassUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;


import java.util.*;

public class JdbcTemplateBase {

    @Autowired
    public JdbcTemplate jdbcTemplate;

    /**
     * 增删改 sql
     * @param sql
     * @return
     */
    public int update(String sql)
    {
        return jdbcTemplate.update(sql);
    }

    /**
     * 生成 Insert Sql
     * @param table     表名
     * @param entity    实体
     * @return String
     */
    public String insertSql(String table,Object entity) throws Exception {
        List<Map<String,String>> list= SimpleClassUtil.getFiledsInfo(entity);
        String names=new String();
        String values=new String();
        for (Map map: list) {
            String name= (map.get("name") == null) ? "" : String.valueOf(map.get("name"));
            String value= (map.get("value") == null) ? "" : String.valueOf(map.get("value"));
            String type= (map.get("type") == null) ? "" : String.valueOf(map.get("type"));
            names+=String.format("`%s`,",name);
            if(type.indexOf("java.util.Date")!=-1)
            {
                value= DateTimeUtil.zoneToLocalTimeStr(value);
                values+=String.format("'%s',",value);
            }
            else
            {
                values+=String.format("'%s',",value);
            }
        }
        names=names.substring(0, names.length()-1);
        values=values.substring(0, values.length()-1);
        String sql=String.format(" insert into %s(%s) values(%s)  ",table,names,values);
        return sql;
    }

    /**
     * 生成 update sql
     * @param table 表名
     * @param entity 实体
     * @param whereNames 非更改列
     * @return String
     * @throws Exception
     */
    public String updateSql(String table,Object entity,String[] whereNames) throws Exception {
        List<Map<String,String>> list= SimpleClassUtil.getFiledsInfo(entity);
        String updateNameValues=new String();
        String whereNameValues=new String();
        for (Map map: list) {
            String name= (map.get("name") == null) ? "" : String.valueOf(map.get("name"));
            String value= (map.get("value") == null) ? "" : String.valueOf(map.get("value"));
            String type= (map.get("type") == null) ? "" : String.valueOf(map.get("type"));
            boolean isWhere=false;
            for (String whereName:
                 whereNames) {
                if(name.toLowerCase().equals(whereName.toLowerCase()))
                {
                    isWhere=true;
                    break;
                }
            }
            if(!isWhere) {
                if(type.indexOf("java.util.Date")!=-1)
                {
                    value= DateTimeUtil.zoneToLocalTimeStr(value);
                    updateNameValues+=String.format("`%s`='%s',",name,value);
                }
                else
                {
                    updateNameValues+=String.format("`%s`='%s',",name,value);
                }
            }
            else {
                whereNameValues+=String.format(" and `%s`='%s' ",name,value);
            }
        }
        updateNameValues=updateNameValues.substring(0, updateNameValues.length()-1);
        String sql=String.format(" update %s set %s where 1=1 %s ",table,updateNameValues,whereNameValues);
        return sql;
    }

    /**
     *生成 deleteSql
     * @param table   表名
     * @param whereNameValues 条件与值
     * @return
     */
    public String deleteSql(String table,Map<String,String> whereNameValues) throws  Exception{
        String wnvStr=new String();
        for (Map.Entry<String,String> map:whereNameValues.entrySet()) {
            wnvStr+=String.format(" and `%s`='%s' ",map.getKey(),map.getValue());
        }
        String sql=String.format(" delete from %s  where 1=1 %s ",table,wnvStr);
        return sql;
    }

    /**
     * 生成 selectSql
     * @param table 表名
     * @param where 实体
     * @return String
     * @throws Exception
     */
    public String selectSql(String table,String where)throws Exception {
        String sql=new String();
        sql+=String.format(" select * from %s where 1=1 %s ",table,where);
        return sql;
    }

}
