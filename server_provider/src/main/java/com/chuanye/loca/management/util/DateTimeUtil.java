package com.chuanye.loca.management.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtil {
    public static String zoneToLocalTimeStr(String stringDate) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
        Date d = sdf.parse(stringDate);
        String formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
        return formatDate;
    }

    public static String getStringDate(java.util.Date date) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(date);
        return dateString;
    }

    public static java.sql.Timestamp addHours(java.sql.Timestamp timestamp,long hours) throws Exception {
        long time= timestamp.getTime()+ 1000 * 3600 * hours;
        return new java.sql.Timestamp(time);
    }

    public static java.sql.Timestamp addMinutes(java.sql.Timestamp timestamp,long minutes)throws Exception {
        long time= timestamp.getTime()+ 1000 * 60 * minutes;
        return new java.sql.Timestamp(time);
    }

    public static java.sql.Timestamp addSeconds(java.sql.Timestamp timestamp,long seconds) throws Exception {
        long time= timestamp.getTime()+ 1000 * seconds;
        return new java.sql.Timestamp(time);
    }

    public static java.sql.Timestamp addMonths(java.sql.Timestamp timestamp,int months) throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(timestamp);
        calendar.add(Calendar.MONTH, months);
        Date date=calendar.getTime();
        return new java.sql.Timestamp(date.getTime());
    }

    public static java.sql.Timestamp addYears(java.sql.Timestamp timestamp,int years) throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(timestamp);
        calendar.add(Calendar.YEAR, years);
        Date date=calendar.getTime();
        return new java.sql.Timestamp(date.getTime());
    }

}
