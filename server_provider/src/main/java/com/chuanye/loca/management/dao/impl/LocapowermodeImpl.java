package com.chuanye.loca.management.dao.impl;

import com.chuanye.loca.management.dao.JdbcTemplateBase;
import com.chuanye.loca.management.dao.LocapowermodeDao;
import com.chuanye.loca.management.model.Locapowermode;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Primary
class LocapowermodeImpl extends JdbcTemplateBase implements LocapowermodeDao {
    public List<Locapowermode> get(long powerMode) throws Exception{
        StringBuilder sql=new  StringBuilder() ;
        sql.append(" select * from locapowermode  where 1=1");
        if(powerMode > 0)
        {
            sql.append(" and `PowerMode`='"+powerMode+"'");
        }
        sql.append(" order by `PowerMode` asc ");
        List<Locapowermode> locapowermodes = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<Locapowermode>(Locapowermode.class));
        return locapowermodes;
    }
}
