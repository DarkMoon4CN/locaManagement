package com.chuanye.loca.management.util;

public class StringUtil {

    /**
     * 字符串是否为空
     * @param str
     * @return true or false
     */
    public static Boolean isNullOrEmpty(String str) {
        if (str == null) {
            return true;
        } else if (str.length() == 0) {
            return true;
        }
        return false;
    }
}
