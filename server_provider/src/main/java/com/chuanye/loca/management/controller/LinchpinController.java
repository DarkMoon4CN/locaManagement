package com.chuanye.loca.management.controller;


import com.chuanye.loca.management.model.Linchpin;
import com.chuanye.loca.management.request.LinchpinGetRequest;
import com.chuanye.loca.management.service.LinchpinService;
import com.chuanye.loca.management.util.ResultFirstDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "linchpin",description = "省市县区镇 级联菜单",tags = {"Linchpin"})
@RestController
@RequestMapping("/linchpin")
public class LinchpinController {
    @Autowired
    private LinchpinService service;

    @ApiOperation(value = "获取单一")
    @PostMapping("/get")
    public ResultFirstDto<Linchpin> getByLinchpinId(@RequestBody LinchpinGetRequest obj) {
        ResultFirstDto<Linchpin> result=new ResultFirstDto<Linchpin>();
        try {
            Linchpin info=service.get(obj.getLinchpinId());
            result.setFirstParam(info);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/linchpin/get Server Error");
        }
        return result;
    }

    @ApiOperation(value = "获取其下级地区")
    @PostMapping("/getsub")
    public ResultFirstDto<List<Linchpin>> getSubByLinchinId(@RequestBody LinchpinGetRequest obj) {
        ResultFirstDto<List<Linchpin>> result=new ResultFirstDto<List<Linchpin>>();
        try {
            List<Linchpin> info=service.sub(obj.getLinchpinId());
            result.setFirstParam(info);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/getSubByLinchinId/get Server Error");
        }
        return result;
    }


}
