package com.chuanye.loca.management.dao.impl;

import com.chuanye.loca.management.dao.JdbcTemplateBase;
import com.chuanye.loca.management.dao.VideoalarmauditstateDao;
import com.chuanye.loca.management.model.Videoalarmauditstate;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Primary
class VideoalarmauditstateImpl extends JdbcTemplateBase implements VideoalarmauditstateDao {
    public List<Videoalarmauditstate> get(long auditState) throws Exception {
        StringBuilder sql=new  StringBuilder() ;
        sql.append(" select * from videoalarmauditstate  where 1=1 ");
        if(auditState > 0)
        {
            sql.append(" and `AuditState`='"+auditState+"'");
        }
        sql.append(" order by `AuditState` asc ");
        List<Videoalarmauditstate> videoalarmauditstates = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<Videoalarmauditstate>(Videoalarmauditstate.class));
        return videoalarmauditstates;
    }
}
