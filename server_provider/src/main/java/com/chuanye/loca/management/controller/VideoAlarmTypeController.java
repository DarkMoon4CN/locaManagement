package com.chuanye.loca.management.controller;

import com.chuanye.loca.management.model.Videoalarmtype;
import com.chuanye.loca.management.service.VideoalarmtypeService;
import com.chuanye.loca.management.util.ResultFirstDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "videoalarmtype",description = "视频告警类型",tags = {"VideoAlarmType"})
@RestController
@RequestMapping("/videoalarmtype")
public class VideoAlarmTypeController {
    @Autowired
    private VideoalarmtypeService service;

    @ApiOperation(value = "获取全部")
    @PostMapping("/getall")
    public ResultFirstDto<List<Videoalarmtype>> getAll() {
        ResultFirstDto<List<Videoalarmtype>> result=new ResultFirstDto<>();
        try {
            List<Videoalarmtype> infos=service.get(0);
            result.setFirstParam(infos);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/videoalarmtype/getall Server Error");
        }
        return result;
    }
}
