package com.chuanye.loca.management.request;

public class VideoalarmGetRequest {
    private String alarmId;

    public String getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(String alarmId) {
        this.alarmId = alarmId;
    }
}
