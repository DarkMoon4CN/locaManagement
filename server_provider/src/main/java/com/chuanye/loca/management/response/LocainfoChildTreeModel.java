package com.chuanye.loca.management.response;

import java.util.List;

public class LocainfoChildTreeModel {


    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public List<LocainfoChildTreeModel> getChild() {
        return child;
    }

    public void setChild(List<LocainfoChildTreeModel> child) {
        this.child = child;
    }

    public List<LocaInfoModel> getList() {
        return list;
    }

    public void setList(List<LocaInfoModel> list) {
        this.list = list;
    }

    private String nodeId;
    private String nodeName;
    private List<LocainfoChildTreeModel> child;
    private java.util.List<LocaInfoModel> list;
}
