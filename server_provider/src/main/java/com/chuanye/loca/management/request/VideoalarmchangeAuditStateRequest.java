package com.chuanye.loca.management.request;

public class VideoalarmchangeAuditStateRequest {
    public String getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(String alarmId) {
        this.alarmId = alarmId;
    }

    public int getAuditState() {
        return auditState;
    }

    public void setAuditState(int auditState) {
        this.auditState = auditState;
    }

    private String alarmId;
    private int auditState;
}
