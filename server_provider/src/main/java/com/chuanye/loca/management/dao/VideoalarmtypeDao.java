package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.model.Videoalarmtype;

import java.util.List;

public interface VideoalarmtypeDao {
    List<Videoalarmtype> get(long alarmType) throws Exception;
}
