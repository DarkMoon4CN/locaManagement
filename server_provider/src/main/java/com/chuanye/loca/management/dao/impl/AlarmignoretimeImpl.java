package com.chuanye.loca.management.dao.impl;

import com.chuanye.loca.management.dao.AlarmignoretimeDao;
import com.chuanye.loca.management.dao.JdbcTemplateBase;
import com.chuanye.loca.management.model.Alarmignoretime;
import com.chuanye.loca.management.response.AlarmignoretimeModel;
import com.chuanye.loca.management.util.DateTimeUtil;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.swing.text.StyledEditorKit;
import java.util.List;


@Repository
@Primary
public class AlarmignoretimeImpl extends JdbcTemplateBase implements AlarmignoretimeDao {
    public int add(Alarmignoretime info) throws Exception {
        String sql=insertSql("alarmignoretime",info);
        return update(sql);
    }

    public List<AlarmignoretimeModel> get() throws Exception {
        String sql=selectSql("alarmignoretime","");
        List<AlarmignoretimeModel>  alarmignoretimeModels = jdbcTemplate.query(sql, new BeanPropertyRowMapper<AlarmignoretimeModel>(AlarmignoretimeModel.class));
        for (AlarmignoretimeModel alarmignoretimeModel:alarmignoretimeModels){
             java.sql.Timestamp alarmNextTime=DateTimeUtil.addMinutes(alarmignoretimeModel.getIgnoreTime(),alarmignoretimeModel.getValue());;
             alarmignoretimeModel.setAlarmNextTime(alarmNextTime);
        }
        return alarmignoretimeModels;
    }
}
