package com.chuanye.loca.management.response;

import com.chuanye.loca.management.model.Alarmignoretime;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;

public class AlarmignoretimeModel extends Alarmignoretime {

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8",
            shape = JsonFormat.Shape.STRING,
            locale ="zh"
    )
    @ApiModelProperty(value ="下一次告警时间",example = "yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp alarmNextTime;

    public Timestamp getAlarmNextTime() {
        return alarmNextTime;
    }

    public void setAlarmNextTime(Timestamp alarmNextTime) {
        this.alarmNextTime = alarmNextTime;
    }
}
