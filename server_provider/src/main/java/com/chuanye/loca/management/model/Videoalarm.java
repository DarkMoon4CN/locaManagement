package com.chuanye.loca.management.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

;


public class Videoalarm {

  private String alarmId;
  private String locaId;
  private long alarmType;
  private String alarmImgPath;
  private double horizontalAngle;
  private double pitchAngle;
  private long nUnit;
  private long auditState;
  private java.sql.Timestamp alarmTime;
  private java.sql.Timestamp addTime;
  private String lpFileName;


  public String getAlarmId() {
    return alarmId;
  }

  public void setAlarmId(String alarmId) {
    this.alarmId = alarmId;
  }


  public String getLocaId() {
    return locaId;
  }

  public void setLocaId(String locaId) {
    this.locaId = locaId;
  }


  public long getAlarmType() {
    return alarmType;
  }

  public void setAlarmType(long alarmType) {
    this.alarmType = alarmType;
  }


  public String getAlarmImgPath() {
    return alarmImgPath;
  }

  public void setAlarmImgPath(String alarmImgPath) {
    this.alarmImgPath = alarmImgPath;
  }


  public double getHorizontalAngle() {
    return horizontalAngle;
  }

  public void setHorizontalAngle(double horizontalAngle) {
    this.horizontalAngle = horizontalAngle;
  }


  public double getPitchAngle() {
    return pitchAngle;
  }

  public void setPitchAngle(double pitchAngle) {
    this.pitchAngle = pitchAngle;
  }


  public long getNUnit() {
    return nUnit;
  }

  public void setNUnit(long nUnit) {
    this.nUnit = nUnit;
  }


  public long getAuditState() {
    return auditState;
  }

  public void setAuditState(long auditState) {
    this.auditState = auditState;
  }

  @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
  @JsonFormat(
          pattern = "yyyy-MM-dd HH:mm:ss",
          timezone = "GMT+8",
          shape = JsonFormat.Shape.STRING,
          locale ="zh"
  )
  @ApiModelProperty(value = "告警时间",example = "yyyy-MM-dd HH:mm:ss")
  public java.sql.Timestamp getAlarmTime() {
    return alarmTime;
  }



  public void setAlarmTime(java.sql.Timestamp alarmTime) {
    this.alarmTime = alarmTime;
  }

  @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
  @JsonFormat(
          pattern = "yyyy-MM-dd HH:mm:ss",
          timezone = "GMT+8",
          shape = JsonFormat.Shape.STRING,
          locale ="zh"
  )
  @ApiModelProperty(value = "增加时间",example = "yyyy-MM-dd HH:mm:ss")
  public java.sql.Timestamp getAddTime() {
    return addTime;
  }

  public void setAddTime(java.sql.Timestamp addTime) {
    this.addTime = addTime;
  }


  public String getLpFileName() {
    return lpFileName;
  }

  public void setLpFileName(String lpFileName) {
    this.lpFileName = lpFileName;
  }

}
