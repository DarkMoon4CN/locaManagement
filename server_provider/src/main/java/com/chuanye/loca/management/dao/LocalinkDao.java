package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.model.Localink;

import java.util.List;

public interface LocalinkDao {
    List<Localink> get(long link) throws Exception;
}
