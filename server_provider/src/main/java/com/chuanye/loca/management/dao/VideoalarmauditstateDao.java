package com.chuanye.loca.management.dao;

import com.chuanye.loca.management.model.Videoalarmauditstate;

import java.util.List;

public interface VideoalarmauditstateDao {
    List<Videoalarmauditstate> get(long auditState) throws Exception;
}
