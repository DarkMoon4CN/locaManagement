package com.chuanye.loca.management.service;

import com.chuanye.loca.management.dao.AlarmignoretimeDao;
import com.chuanye.loca.management.model.Alarmignoretime;
import com.chuanye.loca.management.response.AlarmignoretimeModel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AlarmignoretimeService {

    @Resource
    private AlarmignoretimeDao dao;

    public int addAlarmignoretime(Alarmignoretime info)throws Exception{
        return dao.add(info);
    }

    public List<AlarmignoretimeModel> get() throws Exception {
        return dao.get();
    }
}
