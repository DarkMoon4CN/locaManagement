package com.chuanye.loca.management.service;

import com.chuanye.loca.management.dao.VideoalarmtypeDao;
import com.chuanye.loca.management.model.Videoalarmtype;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.rmi.AlreadyBoundException;
import java.util.List;

@Service
public class VideoalarmtypeService {
    @Resource
    private VideoalarmtypeDao dao;

    public List<Videoalarmtype> get(long alarmType) throws Exception{
        return dao.get(alarmType);
    }

}
