package com.chuanye.loca.management.request;

public class LocalinkGetRequest {
    private  long link;

    public long getLink() {
        return link;
    }

    public void setLink(long link) {
        this.link = link;
    }
}
