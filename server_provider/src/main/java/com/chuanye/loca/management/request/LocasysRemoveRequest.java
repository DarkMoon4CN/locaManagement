package com.chuanye.loca.management.request;

public class LocasysRemoveRequest {
    public String getLocaId() {
        return locaId;
    }

    public void setLocaId(String locaId) {
        this.locaId = locaId;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    private String locaId;
    private String sysId;
}
