package com.chuanye.loca.management.request;

import com.chuanye.loca.management.util.RequestPageBase;

public class LocavideoGetByPageRequest extends RequestPageBase {
    private String videoId;

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    private  String locaId;

    public String getLocaId() {
        return locaId;
    }

    public void setLocaId(String locaId) {
        this.locaId = locaId;
    }
}
