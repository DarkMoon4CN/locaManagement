package com.chuanye.loca.management.controller;

import com.chuanye.loca.management.model.Locapowermode;
import com.chuanye.loca.management.request.LocapowermodeRequest;
import com.chuanye.loca.management.service.LocapowermodeService;
import com.chuanye.loca.management.util.ResultFirstDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@Api(value = "locapowermode",description = "站点供电方式",tags = {"LocaPowerMode"})
@RestController
@RequestMapping("/locapowermode")
public class LocaPowerModeController {

    @Autowired
    private LocapowermodeService service;

    @ApiOperation(value = "获取单一")
    @PostMapping("/get")
    public ResultFirstDto<Locapowermode> getByPowerMode(@RequestBody LocapowermodeRequest obj) {
        ResultFirstDto<Locapowermode> result=new ResultFirstDto<>();
        try {
            List<Locapowermode> infos=service.get(obj.getPowerMode());
            result.setFirstParam(infos!=null &&infos.size()>0?infos.get(0):null);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locapowermode/get Server Error");
        }
        return result;
    }

    @ApiOperation(value = "获取全部")
    @PostMapping("/getall")
    public ResultFirstDto<List<Locapowermode>> getAll() {
        ResultFirstDto<List<Locapowermode>> result=new ResultFirstDto<>();
        try {
            List<Locapowermode> infos=service.get(0);
            result.setFirstParam(infos);
        } catch (Exception e) {
            result.setStatus(500);
            result.setMessage("/locapowermode/getall Server Error");
        }
        return result;
    }
}
